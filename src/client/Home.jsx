import React from 'react';
import './app.css';
import LoveboxImage from './lovebox.svg';

export default () => (
  <div className="container">
    <h1 style={{ marginTop: 0, paddingTop: 15 }}>Hello</h1>
    <h1>Welcome in Lovebox Tech Interview</h1>
    <div style={{ textAlign: 'left' }}>
      <h2>Instructions</h2>
      <span>
        The goal is to create some sort of
        <b> virtual Lovebox</b>
        <span role="img" aria-label="heart"> 💌</span>
      </span>
      <ul>
        <li>
          The front-end is composed of 2 parts
          <strong>in the same page</strong>
          :
          <ul>
            <li>
              The 1st one will be used to send messages and will contain:
              <ul>
                <li>A text input</li>
                <li>
                  A 320x240 canvas where the input value will be written.
                </li>
                <li>
                  A button that will call the &apos;sendMessage&apos; mutation to send
                  the canvas base64 to the server.
                </li>
              </ul>
            </li>
            <li>
              The 2nd one will display all received messages and will contain:
              <ul>
                <li>
                  A list of all messages fetched with the &apos;getMessages&apos; query.
                </li>
              </ul>
            </li>
            <li>
              The &apos;getMessages&apos; query&apos;s response will be saved in the cache.
            </li>
            <li>
              When the &apos;sendMessage&apos; mutation is fired, the cache should be updated
              to add the new message in the &apos;getMessages&apos; response.
            </li>
          </ul>
        </li>
        <li>
          The back-end should handle the following query and mutation:
          <ul>
            <li>A &apos;sendMessage&apos; mutation, to send an image&apos;s base64</li>
            <li>A &apos;getMessages&apos; query, to retrieve sent images</li>
          </ul>
        </li>
      </ul>
      <h4>Additional rules</h4>
      <ul>
        <li>
          We don&apos;t care about the persistence of the messages.
          You don&apos;t have to use any database and can use global vars.
        </li>
        <li>
          You must use client-side Apollo&apos;s caching.
        </li>
      </ul>
      <div>
        {'Zip your code in a file named <first_name>.zip and send it to marhold@lovebox.love.'}
      </div>
    </div>
    <h2>
      Good luck, have fun
      <span role="img" aria-label="heart"> ❤️</span>
    </h2>
    <img
      alt="lovebox"
      src={LoveboxImage}
      style={{
        backgroundColor: '#8A64FF',
        paddingLeft: 30,
        paddingRight: 30,
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius: 30,
        marginTop: -10,
      }}
    />
  </div>
);

# Lovebox tech interview

## Get started

### Clone the repository
```
git clone https://bitbucket.org/lovebox-public/react-use-case.git
```

### Go inside the directory
```
cd node-react
```

### Install dependencies
```
yarn
```

### Start development server
```
yarn dev
```
It will start a NodeJS server, and a React webapp.
The NodeJS server is accessible at: `localhost:8080`.
The React webapp is accessible at: `localhost:3000`.

### Specs
Start the project and open the React webapp to see the specs.

Built with [simple-react-full-stack boilerplate](https://github.com/crsandeep/simple-react-full-stack)